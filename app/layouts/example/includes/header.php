<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>MMVC</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=100">
    <link rel="stylesheet" href="<?= baseURL('resources/shared/css/font-awesome/font-awesome.min.css') ?>">
    <link href="<?= baseURL('resources/shared/css/bootstrap/bootstrap-3.0.0.css') ?>" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- fav -->
    <link rel="shortcut icon" href="assets/ico/favicon.ico">
</head>

<body>
<div class="navbar navbar-static-top">
    <div class="container">
        <a href="#" class="navbar-brand">MMVC</a>
        <ul class="nav navbar-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">CSS <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="#headings">Headings</a></li>
                    <li><a href="#content-formatting">Content</a></li>
                    <li><a href="#tables">Tables</a></li>
                    <li><a href="#forms">Forms</a></li>
                    <li><a href="#images">Images</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Components <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="#dropdowns">Dropdowns</a></li>
                    <li><a href="#input-groups">Input Groups</a></li>
                    <li><a href="#navs">Navs</a></li>
                    <li><a href="#navbar">Navbar</a></li>
                    <li><a href="#pagination">Pagination</a></li>
                    <li><a href="#alerts">Alerts</a></li>
                    <li><a href="#labels">Labels</a></li>
                    <li><a href="#progress">Progress</a></li>
                    <li><a href="#media-object">Media Object</a></li>
                    <li><a href="#list-groups">List Groups</a></li>
                    <li><a href="#panels">Panels</a></li>
                    <li><a href="#wells">Wells</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Source <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="../dist/css/bootstrap.css">bootstrap.css</a></li>
                    <li><a href="../dist/css/bootstrap.min.css">bootstrap.min.css</a></li>
                    <li class="divider"></li>
                    <li><a href="../less/variables.less">variables.less</a></li>
                    <li><a href="../less/theme.less">theme.less</a></li>
                </ul>
            </li>
        </ul>
        <p class="navbar-text pull-right">MMVC <a href="#" target="_blank">Examples</a></p>
    </div>
</div>