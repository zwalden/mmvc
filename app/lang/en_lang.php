<?php
// one file for all translations for a single language?
$en_lang = array(

/* Validation messages */
'VALIDATE_MATCH' => 'The %s must match %s', // fix lol
'VALIDATE_REQUIRED' => 'The %s field is required',
'VALIDATE_VALID_EMAIL' => 'The %s field is required to be a valid email address',
'VALIDATE_MAX_LEN' => 'The %s field needs to be shorter than %s character(s)',
'VALIDATE_MIN_LEN' => 'The %s field needs to be longer than %s character(s)',
'VALIDATE_EXACT_LEN' => 'The %s field needs to be exactly %s character(s) in length',
'VALIDATE_ALPHA' => 'The %s field may only contain alpha characters(a-z)',
'VALIDATE_ALPHA_NUMERIC' => 'The %s field may only contain alpha-numeric characters',
'VALIDATE_ALPHA_DASH' => 'The %s field may only contain alpha characters &amp; dashes',
'VALIDATE_NUMERIC' => 'The %s field may only contain numeric characters',
'VALIDATE_INTEGER' => 'The %s field may only contain a numeric value',
'VALIDATE_BOOLEAN' => 'The %s field may only contain a true or false value',
'VALIDATE_FLOAT' => 'The %s field may only contain a float value',
'VALIDATE_VALID_URL' => 'The %s field is required to be a valid URL',
'VALIDATE_URL_EXISTS' => 'The %s URL does not exist',
'VALIDATE_VALID_IP' => 'The %s field needs to contain a valid IP address',
'VALIDATE_VALID_CC' => 'The %s field needs to contain a valid credit card number',
'VALIDATE_VALID_DATE' => 'The %s field needs to be a valid date',
'VALIDATE_MIN_NUMERIC' => 'The %s field needs to be a numeric value that is equal to, or higher than %s',
'VALIDATE_MAX_NUMERIC' => 'The %s field needs to be a numeric value, equal to, or lower than %s',
'VALIDATE_IS_UNIQUE' => 'The value %s is already in use.',
'VALIDATE_COMMON_STRING' => 'The %s field only accepts alpha, numeric and punctuation.'


);